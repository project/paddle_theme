<?php

/**
 * @file
 * Default simple view template to display a group of summary lines.
 *
 * This wraps items in a span if set to inline, or a div if not.
 *
 * Variables available:
 * - $options: contains style options.
 * - $rows: The results of the view query, if any.
 *
 * @ingroup views_templates
 */
?>
<?php foreach ($rows as $id => $row): ?>
  <?php print (!empty($options['inline']) ? '<span' : '<div') . ' class="views-summary views-summary-unformatted">'; ?>
  <?php if (!empty($row->separator)) { print $row->separator; } ?>
  <a aria-labelledby="<?php print $row->link; ?>" href="<?php print $row->url; ?>"<?php print !empty($row_classes[$id]) ? ' class="' . $row_classes[$id] . '"' : ''; ?>><?php print $row->link; ?></a>
  <?php if (!empty($options['count'])): ?>
    (<?php print $row->count; ?>)
  <?php endif; ?>
  <?php print !empty($options['inline']) ? '</span>' : '</div>'; ?>
<?php endforeach; ?>
