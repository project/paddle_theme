<?php

/**
 * @file
 * Template that displays the teaser version of a researcher.
 */
?>
<a href="/node/<?php print $nid ?>">
  <div class="researcher-teaser" data-researcher-nid="<?php print $nid; ?>">
    <div class="researcher-teaser-image col-md-2">
      <?php if (!empty($photo1)) : ?>
        <?php print $photo1; ?>
        <?php if (!empty($photo2)) : ?>
          <?php print $photo2; ?>
        <?php endif; ?>
      <?php else: ?>
        <div class="no-image" role="img" aria-label="<?php print t('No image available'); ?>"></div>
      <?php endif; ?>
    </div>
    <div class="researcher-teaser-content col-md-10">
      <h3>
        <?php print $title; ?>
      </h3>
      <?php if (!empty($position)): ?>
        <div class="position">
          <?php print $position; ?>
        </div>
      <?php endif; ?>
      <?php if (!empty($email) || !empty($telephone)): ?>
        <div class="contact">
          <?php if (!empty($email)): ?>
            <div class="email">
              <i class="fa fa-envelope"></i>
              <?php print $email; ?>
            </div>
          <?php endif; ?>
          <?php if (!empty($telephone)): ?>
            <div class="telephone">
              <i class="fa fa-phone"></i>
              <?php print $telephone; ?>
            </div>
          <?php endif; ?>
        </div>
      <?php endif; ?>
    </div>
  </div>
</a>
