<?php

/**
 * @file
 * Template that displays the spotlight version of a event page.
 */
?>
<div class="pane-section-body view-mode-spotlight">
  <a href="<?php print $output['link']; ?>">
    <div class="node-spotlight">
      <div class="node-spotlight-image">
        <?php print $output['image']; ?>
      </div>
      <div class="node-content">
        <div class="node-header">
          <?php if (!empty($event_date)): ?>
          <h5>
            <?php print $event_date; ?>
          </h5>
          <?php endif; ?>
          <h4>
            <?php print $output['title']; ?>
          </h4>
        </div>
        <?php if (!empty($output['text'])): ?>
          <p>
            <?php print $output['text']; ?>
          </p>
        <?php endif; ?>
      </div>
    </div>
  </a>
</div>
