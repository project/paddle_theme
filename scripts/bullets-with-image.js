/**
 * @file
 * Contains js code for styling the bullets with left aligned images.
 */
(function ($) {
  /**
   * Initializes the elements.
   */
  Drupal.behaviors.bulletsWithImage = {
    attach: function (context, settings) {
      // Set bullets styling with left aligned images.
      $('img[style*="float:left"]').parent().siblings('ul').css('display', 'inline-block');
    }
  };
})(jQuery);
