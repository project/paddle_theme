/**
 * @file
 * JS to init Sticky with the proper settings.
 */

(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.sticky = {
    attach: function (context, settings) {
      var config = settings.sticky;

      // Check if user filled out valid selector.
      if ($("html").find(config.selector).length) {
        $(config.selector).sticky({
          selector: config.selector,
        });

          $(config.selector).on("sticky-start", function () {
            if (config.topSpacing) {
              $(config.selector).css("margin-top", config.topSpacing);
            }
            $(config.selector).css("padding-top", "15px");
          });

          $(config.selector).on("sticky-end", function () {
            if (config.topSpacing) {
              $(config.selector).css("margin-top", 0);
            }
            $(config.selector).css("padding-top", 0);
          });
        }

    }
  };
})(jQuery, Drupal);
